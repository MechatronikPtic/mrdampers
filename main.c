/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "can.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
	CAN_HandleTypeDef hcan;
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	uint16_t adc_values[2];
	uint16_t pwm_values[2];
	int16_t acc_x [5] = {0,0,0,0,0}; //longitudal acc
	int16_t acc_y [5] = {0,0,0,0,0}; //lateral acc
	int8_t der_acc_x;
	uint8_t brake_pr, throtle, speed;
	int16_t steering_wheel;
	int16_t fK;
	uint8_t timestamp;
	int8_t con1,con2;
	uint8_t msg[8] = {0,0,0,0,0,0,0,0};
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */


  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_CAN_Init();
  MX_TIM2_Init();
  MX_USART1_UART_Init();
  MX_TIM3_Init();
  //CAN setup
CAN_RxHeaderTypeDef rxHeader; //CAN Bus Transmit Header
CAN_TxHeaderTypeDef txHeader; //CAN Bus Receive Header
uint8_t canRX[8] = {0,0,0,0,0,0,0,0};  //CAN Bus Receive Buffer
//uint8_t canRX_copy[8] = {0,0,0,0,0,0,0,0};  //CAN Bus Receive Buffer
CAN_FilterTypeDef canfil; //CAN Bus Filter
uint32_t canMailbox; //CAN Bus Mail box variable

canfil.FilterBank = 0;
canfil.FilterMode = CAN_FILTERMODE_IDMASK;
canfil.FilterFIFOAssignment = CAN_RX_FIFO0;
canfil.FilterIdHigh = 0;
canfil.FilterIdLow = 0;
canfil.FilterMaskIdHigh = 0;
canfil.FilterMaskIdLow = 0;
canfil.FilterScale = CAN_FILTERSCALE_32BIT;
canfil.FilterActivation = ENABLE;
canfil.SlaveStartFilterBank = 14;

txHeader.DLC = 8; // Number of bites to be transmitted max- 8
txHeader.IDE = CAN_ID_STD;
txHeader.RTR = CAN_RTR_DATA;
txHeader.StdId = 0x030;
txHeader.ExtId = 0x02;
txHeader.TransmitGlobalTime = DISABLE;

HAL_CAN_ConfigFilter(&hcan,&canfil); //Initialize CAN Filter
HAL_StatusTypeDef f = HAL_CAN_Start(&hcan); //Initialize CAN Bus
//HAL_CAN_ActivateNotification(&hcan,CAN_IT_RX_FIFO0_MSG_PENDING);// Initialize CAN Bus Rx Interrupt

  /* USER CODE BEGIN 2 */

  HAL_TIM_PWM_MspInit(&htim2); //5kHz, 3600 res
  HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_4);
  HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_3);
  HAL_ADC_Start_DMA(&hadc1, (uint32_t*)adc_values, 2);
  HAL_TIM_Base_Start_IT(&htim3);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  // Get PWM value
	  	  pwm_values[0] = (uint16_t)(adc_values[1]);
	  	  pwm_values[1] = (uint16_t)(adc_values[1]);

	  	// Covert to string and print
	  		  sprintf(msg, "%hu, %hu, %hu\n", adc_values[1], pwm_values[0], pwm_values[1]);
	  		  HAL_UART_Transmit(&huart1, (uint8_t*)msg, strlen(msg), HAL_MAX_DELAY);
	  	//CAN get msg
	  		  HAL_CAN_GetRxMessage(&hcan, CAN_RX_FIFO0, &rxHeader, canRX);
	  		  timestamp = canRX[6];
	  		  acc_x [timestamp%5] = canRX[0];
	  		  brake_pr = canRX[1];
	  		  throtle = canRX[2];
	  		  speed = canRX[3];
	  		  steering_wheel = canRX[4];
	  		  acc_y[timestamp%5] = canRX[5];

	    //acc_x der
	  		  der_acc_x = 0;
	  		  for(int i = 1;i<4;i++){
	  			  der_acc_x += acc_x[i]-acc_x[i-1];
	  		  }
	  		  der_acc_x = der_acc_x/5;
	  	//fK
	  		  if(speed > 60) con1= -1;
	  		  else con1 = 1;

	  		  if(throtle <= 45) con1 *= -1;

	  		  int16_t product = steering_wheel * acc_y[timestamp%5];
	  		  if( product > 0) con1 = product;
	  		  else con1 *= product;

	  		  if(con1 >= 0 ) con1 = 0;
	  		  else con1 = -1;

	  		  if(acc_x[timestamp%5] >=0) con2 = 0;
	  		  else con2 = 1;

	  		  fK = steering_wheel*steering_wheel*con1*con2;

	  	//TPS_Y
	  		  int TPS_Y;
	  		  if(abs(fK) <= 1) TPS_Y = speed*(-5) + 300;
	  		  else TPS_Y = 0;

	  	//Current/PWM
	  		  if(der_acc_x ==0)
	  			pwm_values[0] = fK + abs(5*acc_x[timestamp%5])*3/2+143;
	  		  if(der_acc_x < 0)
	  			pwm_values[0] = fK + abs(5*acc_x[timestamp%5]+16*(abs(der_acc_x)+brake_pr))*3/2+143;
	  		  if(der_acc_x > 0)
	  			pwm_values[0] = fK + abs(5*acc_x[4]+10*(der_acc_x+TPS_Y))*3/2+143;

	    // Set PWM value @PRSC1 Period 7200
	  		__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, pwm_values[0]);
	  		//__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, canRX[0]);
	  		  /*	+22uH
	  		   DUTY 129	~ 	0.5A
	  		   DUTY 160 ~ 	0.76A
	  		   DUTY 198 ~ 	1A
	  		   DUTY 300	~ 	1.5A
	  		   DUTY 460	~ 	2A
	  		   */
	  		//__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_3, pwm_values[1]);
	  		__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_3, canRX[7]);
	  		  /* 	+22uH
	  		   DUTY 123	~ 	0.5A
	  		   DUTY 157	~ 	0.76A
	  		   DUTY 190	~ 	1A
	  		   DUTY 297	~ 	1.5A
	  		   DUTY 444	~ 	2A
	  		   */

	    //CAN send msg
	  		uint8_t csend[] = {0x01,0x02,0x03,pwm_values[1],0x05,0x06,0x07,0x08}; // Tx Buffer
	  		HAL_CAN_AddTxMessage(&hcan,&txHeader,csend,&canMailbox); // Send Message

	  	HAL_Delay(300);


	  	/*if(canRX[0] == 10){
	  		HAL_UART_Transmit(&huart1, (uint8_t*)ok, strlen(ok), HAL_MAX_DELAY);
	  	}
	  	else{
	  		HAL_UART_Transmit(&huart1, (uint8_t*)not_ok, strlen(not_ok), HAL_MAX_DELAY);
	  	}*/

	  	HAL_Delay(300);

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV8;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
